﻿using UnityEngine;

public class BaseController : MonoBehaviour
{
    public float speed = 1;
    public float rotationSpeed = 1;
    public float towerRotationSpeed = 1;
    public float shellSpeed = 10;
    public float reloadingSpeed = 1;
    public float mass = 1000;
    
    protected bool isReloading = false;
    protected bool isMoving = false;
	
	[SerializeField]
    protected GameObject shell;

    [SerializeField]
    protected GameObject gun;

	[SerializeField]
    protected GameObject tower;

    public virtual void move() {}
    public virtual void move(bool flag) { }
    public virtual void rotateTower() {}
    public virtual void rotateTower(bool flag) {}

    protected virtual void fire() { 
		GameObject shell = Instantiate(this.shell, gun.transform.position, gun.transform.rotation);
        shell.GetComponent<Rigidbody>().AddForce(shell.transform.forward * shellSpeed);
        isReloading = true;
        Invoke("reload", reloadingSpeed);
	}

    protected virtual void reload() { 
		isReloading = false;
	}
}
