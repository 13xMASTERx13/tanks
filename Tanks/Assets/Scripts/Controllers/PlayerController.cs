﻿using UnityEngine;

public class PlayerController : BaseController
{
	[SerializeField]
	private TankJoystickController tankJoystick;
	[SerializeField]
	private TowerJoystickController towerJoystick;

	private bool isMoved = false, isRotated = false;
	private Quaternion towerRotate;

	private void FixedUpdate()
	{
		if (isMoved)
		{
			Vector3 rightVector = new Vector3(tankJoystick.getHorizontal(), 0f, 0f);
			Vector3 forwardVector = new Vector3(0f, 0f, tankJoystick.getVertical());

			if (forwardVector.z > 0f && rightVector.x > 0f)
			{
				transform.position += forwardVector * speed * Time.deltaTime;
				transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, transform.right,
																				rotationSpeed * Time.deltaTime, 0f));
			}
			else if (forwardVector.z > 0f && rightVector.x < 0f)
			{
				transform.position += forwardVector * speed * Time.deltaTime;
				transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, -transform.right,
																				rotationSpeed * Time.deltaTime, 0f));
			}
			else if (forwardVector.z < 0f && rightVector.x < -0f)
			{
				transform.position -= forwardVector * speed * Time.deltaTime;
				transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, -transform.right,
																				rotationSpeed * Time.deltaTime, 0f));
			}
			else if (forwardVector.z < 0f && rightVector.x > -0f)
			{
				transform.position -= forwardVector * speed * Time.deltaTime;
				transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, transform.right,
																				rotationSpeed * Time.deltaTime, 0f));
			}
			else if (forwardVector.z > 0f)
			{
				tower.transform.rotation = towerRotate;
				transform.position += forwardVector * speed * Time.deltaTime;

				if (Vector3.Angle(tower.transform.forward, transform.forward) > 1f)
					transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, tower.transform.forward,
																					rotationSpeed * Time.deltaTime, 0f));
			}
			else if (forwardVector.z < -0f)
			{
				tower.transform.rotation = towerRotate;
				transform.position -= forwardVector * speed * Time.deltaTime;

				if (Vector3.Angle(tower.transform.forward, transform.forward) > 1f)
					transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, tower.transform.forward,
																					rotationSpeed * Time.deltaTime, 0f));
			}
		}

		if (isRotated)
		{
			tower.transform.RotateAround(Vector3.up, towerJoystick.getHorizontal() * towerRotationSpeed * Time.deltaTime);
			towerRotate = tower.transform.rotation;
		}
	}

	public override void move(bool flag)
	{
		isMoved = flag;
	}

	public void rotateTower(bool flag)
	{
		isRotated = flag;
	}

	protected override void fire()
	{
		throw new System.NotImplementedException();
	}

	protected override void reload()
	{
		throw new System.NotImplementedException();
	}
}
