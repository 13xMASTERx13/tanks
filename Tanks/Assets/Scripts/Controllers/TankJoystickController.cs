﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TankJoystickController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image joystickBG, joystick;
	private Vector2 inputVector;

	void Start()
	{
		joystickBG = GetComponent<Image>();
		joystick = transform.GetChild(0).GetComponent<Image>();
	}

	public float getHorizontal()
	{
		return inputVector.x != 0f ? inputVector.x : 0f;
	}

	public float getVertical()
	{
		return inputVector.y != 0f ? inputVector.y : 0f;
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		Vector2 joystickPosition;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBG.rectTransform, eventData.position,
				eventData.pressEventCamera, out joystickPosition))
		{
			joystickPosition.x /= joystickBG.rectTransform.sizeDelta.x;
			joystickPosition.y /= joystickBG.rectTransform.sizeDelta.y;

			inputVector = new Vector2(joystickPosition.x, joystickPosition.y);
			inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;

			joystick.rectTransform.anchoredPosition = new Vector2(inputVector.x * (joystickBG.rectTransform.sizeDelta.x / 2),
				inputVector.y * (joystickBG.rectTransform.sizeDelta.y / 2));
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		OnDrag(eventData);
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		inputVector = Vector2.zero;
		joystick.rectTransform.anchoredPosition = Vector2.zero;
	}
}
