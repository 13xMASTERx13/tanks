﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TowerJoystickController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image joystickBG, joystick;
	private Vector2 inputVector;

	void Start()
	{
		joystickBG = GetComponent<Image>();
		joystick = transform.GetChild(0).GetComponent<Image>();
	}

	public float getHorizontal()
	{
		return inputVector.x != 0f ? inputVector.x : 0f;
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		Vector2 joystickPosition;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBG.rectTransform, eventData.position,
				eventData.pressEventCamera, out joystickPosition))
		{
			joystickPosition.x /= joystickBG.rectTransform.sizeDelta.x;

			inputVector = new Vector2(joystickPosition.x, 0f);
			inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;

			joystick.rectTransform.anchoredPosition = new Vector2(inputVector.x * (joystickBG.rectTransform.sizeDelta.x / 2), 0f);
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		OnDrag(eventData);
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		inputVector = Vector2.zero;
		joystick.rectTransform.anchoredPosition = Vector2.zero;
	}
}
