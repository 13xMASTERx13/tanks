﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyController : BaseController
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private float minDistance = 100;

    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance > minDistance && !isMoving)
        {
            
            agent.enabled = true;
            agent.SetDestination(player.transform.position);
            isMoving = true;
        }
        else
        {
            agent.enabled = false;
            isMoving = false;
        }
    }

    void FixedUpdate()
    {
        // Target tower direction
        Vector3 targetDirection = Vector3.RotateTowards(
            tower.transform.forward, player.transform.position - transform.position, 15, 0.0f
        );

        // Rotate tower if can't shoot a player, else if can shoot but don't see player then wait, else shoot
        if (Vector3.Angle(tower.transform.forward, targetDirection) > 2f)
        {
            rotateTower();
        }
        else
        {
            RaycastHit hit;
            if (Physics.Raycast(gun.transform.position, gun.transform.forward, out hit))
            {
                Debug.DrawRay(gun.transform.position, gun.transform.forward*1000, Color.green);
                if (hit.collider.gameObject.GetComponent<PlayerController>() && !isReloading)
                {
                    fire();
                }
            }
        }
    }

    public override void rotateTower()
    {
        // Delta tower direction
        Vector3 newDirection = Vector3.RotateTowards(
            tower.transform.forward, player.transform.position - transform.position, towerRotationSpeed * Time.deltaTime, 0.0f
        );

        // Set delta tower rotation
        tower.transform.rotation = Quaternion.Euler(
            tower.transform.rotation.eulerAngles.x,
            Quaternion.LookRotation(newDirection).eulerAngles.y,
            tower.transform.rotation.eulerAngles.z
        );
    }
}
