﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other) {
        if (!other.CompareTag(this.tag)) {
            Destroy(gameObject);
        } else {
            Destroy(gameObject, 10);
        }
    }

    
}
